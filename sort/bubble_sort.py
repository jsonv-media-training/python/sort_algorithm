# https://www.youtube.com/watch?v=YHm_4bVOe1s&list=PLj8W7XIvO93rJHSYzkk7CgfiLQRUEC2Sq&index=3&ab_channel=JoeJamesJoeJames
# O(n^2)
"""
    (1):    move 0 -> end
    (2):        move 0 -> end - curr_index(1)
                    swap if curr_index(2) value > curr_index(2) + 1 value
"""
import copy


class BubbleSort:
    def __init__(self, elements: list):
        self.elements = copy.deepcopy(elements)
        self.len = len(elements)

    def sort(self):
        is_swap = True
        for i in range(0, self.len - 1):
            if not is_swap:
                continue
            is_swap = False
            for j in range(0, self.len - 1 - i):
                if self.elements[j] > self.elements[j+1]:
                    self.elements[j], self.elements[j+1] = self.elements[j+1], self.elements[j]
                    is_swap = True

        return self.elements
