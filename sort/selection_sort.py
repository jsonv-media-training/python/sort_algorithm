# https://www.youtube.com/watch?v=mI3KgJy_d7Y&list=PLj8W7XIvO93rJHSYzkk7CgfiLQRUEC2Sq&index=2&ab_channel=JoeJamesJoeJames
# O(n^2)
"""
    (1):    move 0 -> end
                keep tab of min_index
                if curr_index(1) value < min_index value
                    update min_index
                swap if min_index changes
"""
import copy


class SelectionSort:
    def __init__(self, elements: list):
        self.elements = copy.deepcopy(elements)
        self.len = len(elements)

    def sort(self) -> list:
        for i in range(0, self.len - 1):
            min_index = i
            for j in range(i + 1, self.len):
                if self.elements[min_index] > self.elements[j]:
                    min_index = j
            if min_index != i:
                self.elements[i], self.elements[min_index] = self.elements[min_index], self.elements[i]

        return self.elements
