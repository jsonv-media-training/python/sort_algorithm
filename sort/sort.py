import abc
from abc import ABC
from typing import Any


class Sort(object, metaclass=abc.ABCMeta):
    elements: list[Any] = None

    @property
    @abc.abstractmethod
    def len(self):
        raise NotImplementedError()

    @abc.abstractmethod
    def compare(self, i: int, next_i: int, *args, **kwargs) -> bool:
        raise NotImplementedError()

    @abc.abstractmethod
    def swap(self, i: int, next_i: int, *args, **kwargs) -> None:
        raise NotImplementedError()

    @abc.abstractmethod
    def sort(self) -> list[Any]:
        raise NotImplementedError()
