# https://www.youtube.com/watch?v=Nkw6Jg_Gi4w&list=PLj8W7XIvO93rJHSYzkk7CgfiLQRUEC2Sq&ab_channel=JoeJames
# O(n^2)
"""
    (1):    move 1 -> len
    (2):        move curr_index(1) -> start
                    swap if curr_index(2) value < curr_index(2) - 1 value

"""
import copy
from typing import Any


class InsertionSort:
    def __init__(self, elements: list[Any]):
        self.elements = copy.deepcopy(elements)
        self.len = len(self.elements)

    def sort(self) -> list:
        # move right, from: [second element] -> end
        for i in range(1, self.len):
            # move left, from: [left element] next to [current] -> start
            for j in range(i-1, -1, -1):
                if self.elements[j] > self.elements[j+1]:
                    self.elements[j], self.elements[j+1] = self.elements[j+1], self.elements[j]
                else:
                    break
        return self.elements


# Optimized
class InsertionSortWithShift:
    def __init__(self, elements: list[Any]):
        self.elements = copy.deepcopy(elements)
        self.len = len(self.elements)

    def sort(self) -> list:
        # move right, from: [second element] -> end
        for i in range(1, self.len):
            curr = self.elements[i]
            # move left, from: [left element] next to [current] -> start
            for j in range(i - 1, -1, -1):
                if self.elements[j] > curr:
                    self.elements[j+1] = self.elements[j]
                else:
                    self.elements[j+1] = curr
                    break
                if j == 0 and self.elements[j] > curr:
                    self.elements[j] = curr
        return self.elements
