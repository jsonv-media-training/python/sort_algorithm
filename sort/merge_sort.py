# https://www.youtube.com/watch?v=cVZMah9kEjI&ab_channel=FelixTechTips
# O(n log n)
"""
    divide to single element
    merge two part at a time while comparing them
    call recursively
"""
import copy


class MergeSort:
    def __init__(self, elements: list):
        self.elements = copy.deepcopy(elements)
        self.len = len(elements)

    def __sort(self, sub_arr: list):
        if len(sub_arr) > 1:
            middle_index = len(sub_arr) // 2  # divide and round to floor
            left_part = sub_arr[:middle_index]
            right_part = sub_arr[middle_index:]

            self.__sort(left_part)
            self.__sort(right_part)
            self.__merge(sub_arr, left_part, right_part)

    def __merge(self, sub_arr: list, left_part: list, right_part: list):
        # keep track of index from each part
        left_index = right_index = curr_index = 0

        # compare left & right then merge back to array
        while left_index < len(left_part) and right_index < len(right_part):
            if left_part[left_index] < right_part[right_index]:
                sub_arr[curr_index] = left_part[left_index]
                left_index += 1
            else:
                sub_arr[curr_index] = right_part[right_index]
                right_index += 1
            curr_index += 1

        # in case left still has elements:
        while left_index < len(left_part):
            sub_arr[curr_index] = left_part[left_index]
            left_index += 1
            curr_index += 1

        # in case right still has elements:
        while right_index < len(right_part):
            sub_arr[curr_index] = right_part[right_index]
            right_index += 1
            curr_index += 1

    def sort(self):
        self.__sort(self.elements)
        return self.elements
