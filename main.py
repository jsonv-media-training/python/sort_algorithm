from sort.bubble_sort import BubbleSort
from sort.insertion_sort import InsertionSort, InsertionSortWithShift
from sort.merge_sort import MergeSort
from sort.selection_sort import SelectionSort

if __name__ == '__main__':
    elements = [1, 6, 7, 8, 5, 4, 9, 2, 1, 10, 0, 200]
    insert_sorted_element = InsertionSort(elements).sort()
    insert_sorted_with_shifting_element = InsertionSortWithShift(elements).sort()
    select_sort_element = SelectionSort(elements).sort()
    bubble_sort_element = BubbleSort(elements).sort()
    merge_sort_element = MergeSort(elements).sort()

    print(elements)
    print(insert_sorted_element)
    print(insert_sorted_with_shifting_element)
    print(select_sort_element)
    print(bubble_sort_element)
    print(merge_sort_element)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
